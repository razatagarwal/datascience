####################
Fruit Classification
####################

============
Introduction
============

The project focuses on the classification of fruits, given their image, from 95 different fruits. The dataset is taken from https://www.kaggle.com/moltean/fruits. Applying different classification models to find the best one.

=======
Dataset
=======

Total number of images: 65429.

Training set size: 48905 images (one fruit per image).

Test set size: 16421 images (one fruit per image).

Number of classes: 95 (fruits).

Image size: 100x100 pixels.

==========
Setting up
==========

Run the following command to install the required packages

``make requirements``


============
Execute Code
============

``make run``
